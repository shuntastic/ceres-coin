<h1>Founders &amp; Board</h1>
<div class="itemList">
    <div class="item">
        <div id="ganderson" class="bioImage"></div>
        <div class="bioText">
            <h3>Greg Anderson</h3>
            <h4>Founder and CEO</h4>
            <p>Greg was a successful real estate builder and developer in Texas for more than 20 years, having completed over $100mm in real estate transactions across Central and Southeast Texas and currently resides in Austin. As a serial entrepreneur, he has had multiple successful exits in various real estate companies. Past participation includes Texas Association of Business, Rotary International, The Bunker Labs, Greater Houston and Austin Builders’ Associations, Texas Military Officer’s Association, the Southern Living Builder’s Council and is currently an Ambassador of the Folded Flag Foundation. He is a graduate of the United States Military Academy.<br /><a href="https://www.linkedin.com/in/greg-anderson-7539821/" target="_blank">LinkedIn</a></p>
        </div>
    </div>
    <div class="item">
        <div id="cuchill" class="bioImage"></div>
        <div class="bioText">
            <h3>Charlie Uchill</h3>
            <h4>Founder and COO</h4>
            <p>Prior to CoolMellon, Charlie was a consultant and Portfolio Manager for Institutional Investors and ran a $150mm equity derivative arbitrage portfolio. He has extensive experience in trading and crafting structured financial products and market linked investments including principal protected notes as well as a variety of hybrid securities, combining elements of debt and equity securities. He served in the United States Operations Command as an officer in the 75<sup>th</sup> Ranger Regiment. He currently resides in St. Charles, Illinois with his wife and 3 children. He is a graduate of the United States Military Academy.
            <br /><a href="https://www.linkedin.com/in/charlieuchill/" target="_blank">LinkedIn</a></p>
        </div>
    </div>
    <div class="item">
        <div id="smcnamara" class="bioImage"></div>
        <div class="bioText">
            <h3>Sean McNamara</h3>
            <h4>CTO</h4>
            <p>After working in key roles for Bank of America, NYSE, and Jump Trading, Sean comes to CoolMellon excited to push the limits of innovation on the technical and economic front. With over 20 years of experience as a technology professional, Sean has handled all aspects of the technical spectrum from development to systems administration, network, and operations management. He holds a Bachelors and Masters in Computer Science, both from DePaul University, and resides in Chicago with his wife and daughter.<br /><a href="https://www.linkedin.com/in/semcnamara/" target="_blank">LinkedIn</a></p>
        </div>
    </div>
</div>
<h2>Board of Advisors</h2>
<div class="itemList">

    <div class="item">
        <div id="tzeoli" class="bioImage"></div>
        <div class="bioText">
            <h3>ANTHONY ZEOLI</h3>
            <p>Anthony Zeoli is a Partner in the Corporate Practice Group at Freeborn and concentrates his practice in the areas of banking and commercial finance, securities, real estate, and general corporate law. Anthony’s commercial finance practice includes the representation of borrowers and lenders in: secured and unsecured lending transactions; corporate reorganizations and restructuring; syndicated commercial financing transactions; and loan workouts. Anthony also has a vibrant securities and general corporate practice including initial and subsequent private debt and equity offerings; mergers and acquisitions; company formation and governance; and general contract drafting and negotiation. He is also an industry leader in the area of crowdfunding, in particular with respect to real estate crowdfunding, peer-to-peer (P2P) lending, and Regulation A+ offerings. He has most recently drafted a bill to allow for an intrastate crowdfunding exemption in Illinois which was unanimously passed by the Illinois House of Representatives and the Illinois Senate. –azeoli@freeborn.com</p>
        </div>
    </div>
    <div class="item">
        <div id="luchill" class="bioImage"></div>
        <div class="bioText">
            <h3>LAWRENCE UCHILL</h3>
            <p>Before starting Uchill Law in 2013, Mr. Uchill was a partner in DLA Piper’s real estate practice group. Prior to joining DLA Piper, he was with Brown Rudnick Berlack Israels LLP. Mr. Uchill’s practice focuses on real estate capital markets, including cross-border investments, portfolio transactions, mezzanine lending and structured finance transactions. His clients include both domestic and foreign investors. He has handled all aspects of real estate financings, acquisitions, dispositions and development involving private sector and governmental parties and commercial, military housing, apartment, retail, hotel, energy and mixed use real estate projects. Mr. Uchill graduated from Boston University School of Law, magna cum laude. -- larry.uchill@uchill-law.com</p>
        </div>
    </div>
    <div class="item">
        <div id="psaladino" class="bioImage"></div>
        <div class="bioText">
            <h3>PETER SALADINO</h3>
            <p>Peter is a passionate entrepreneur and Director of Golden Leaf Holdings. He has built and managed a variety of successful companies. He founded BMF Washington, one of the largest cannabis producer processors in the state of Washington. He has designed and developed turnkey marijuana facilities in Seattle and Raymond, Washington. In addition, Peter is the principal owner of the South Fork Business Park which is a 20 acre site zoned exclusively for cannabis production. He helped found the Washington CannaBusiness Association which has played a significant role in supporting the growth and evolution of the legal cannabis industry in Washington. Peter is also currently President of Charter Construction, a multi-state construction company with over 300 employees and sales of $150,000,000 annually. He is a graduate from the University of Washington and sits on the board of Big Brothers and Big Sisters of King County – peter@chartercon.com</p>
        </div>
    </div>
    <div class="item">
        <div id="jimschultz" class="bioImage"></div>
        <div class="bioText">
            <h3>J.M. “Jim” Schultz</h3>
            <p>Jim Schultz is the founder of Open Prairie and is responsible for the management of four private equity funds with investments in innovative technologies spanning agriculture, medical devices, and information systems.  Funds invested by Open Prairie have resulted in two IPO’s and created over 4,000 jobs across 22 portfolio investments.</p>
            <p>Jim is a fifth-generation Illinoisan, agribusiness entrepreneur, and private equity executive.  Like his ancestors, Jim has continued his family legacy in agribusiness throughout the Midwest with investments in agri-tech companies, rural businesses and farmland.  In his hometown of Effingham, Illinois, Jim developed and created in a bean field an office park, Network Centre, that created over 1,500 professional jobs for college graduates. He gained extensive leadership experience with growth-stage capital expansion over the years at Open Prairie Ventures and Telemind Capital Corporation.  Jim has served as the financial expert on mergers and acquisitions for clients in rural America in software development, banking, manufacturing, retail, healthcare, and entertainment.</p>
            <p>From 2015-2017, Jim served in newly-elected State of Illinois Governor Bruce Rauner’s cabinet as the Director of the Illinois Department of Commerce and Economic Opportunity where he oversaw the following departments:   business development, job training, energy, film, and tourism.  In conjunction with this role, he co-founded Intersect Illinois with Governor Rauner and served as Chairman/CEO, creating the State’s first private economic development corporation to represent the Illinois in job creation and business development.  He travelled on behalf of Governor Rauner leading Jobs Missions to Japan, China, Canada, Germany and France.</p>
            <p>Jim is a recognized leader throughout the State of Illinois. He has served as Chairman of the Board for Prime Banc Corporation – a multi-branch rural banking group serving southern and central Illinois – which grew organically from $40 million in assets to over $500 million in assets…all in rural communities in Illinois.  He also has served as Chairman of the Board for the following organizations:  Illinois Chamber of Commerce, Southeast Illinois Community Foundation, The Cross Foundation, and Effingham County Community Foundation.  Jim also served on the Advisory Board for the Chicago Federal Reserve.</p>
            <p>Jim earned his MBA in Finance and Entrepreneurship from Northwestern University, a Juris Doctor Degree in Corporate Finance from DePaul University, and a Bachelor of Business Administration from Southern Methodist University (cum laude).  Jim was born in Teutopolis, Illinois, and he currently lives in Effingham, Illinois, where he and his wife Laura raised their three sons.</p>

        </div>
    </div>
</div>