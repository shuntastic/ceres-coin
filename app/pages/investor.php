<div id="homeBG" class="TL absFull"></div>
<div class="TL absFull">
	<div id="homeCTA" class="TL absFull">
	<div id="introBlock" class="TL">
		<h1 id="headline" >The first banking<br />ecosystem for the<br />legal cannabis<br />industry.</h1>
		<p><span>The investment:&nbsp;&nbsp;</span>The current CERES Reg D offering<br />for accredited investors seeks to raise $20M.</p> 
		<p><a id="playVideo">Play Video <span></span></a></p>
	</div>
	</div>
</div>
<div id="videoOverlayWrapper" class="TL absFull">
	<div id="videoOverlay" class="TL">
		<div id="videoShell" class="absFull"></div>
	</div>
	<div id="videoOverlayClose" class="TR sectionClose open">
		<img src="images/close_btn.png" />
	</div>
</div>
