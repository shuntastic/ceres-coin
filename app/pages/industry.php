<div id="homeBG" class="TL absFull"></div>
<div class="TL absFull">
	<iframe id="ytPlayer" class="TL absFull hidden" frameborder="0" allowfullscreen="1" title="CERES" width="100%" height="100%" src="https://www.youtube.com/embed/jxFAA9cmmxg?autohide=1&amp;autoplay=0&amp;controls=1&amp;enablejsapi=1&amp;modestbranding=0&amp;rel=0&amp;showinfo=0&amp;wmode=transparent&amp;widgetid=1"></iframe>
	<div id="homeCTA" class="TL absFull">
	<div id="introBlock" class="TL">
		<h1 id="headline" >The first banking<br />ecosystem for the<br />legal cannabis<br />industry.</h1>
		<h2>The investment</h2>
		<p>The current CERES Reg D offering for accredited investors seeks to raise $20M.</p> 
		<a id="playVideo" class="HC">Play Video <span></span></a>
	</div>
	</div>
</div>
