<?php
if(!isset($path)){
  $domain = ((!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "") ? 'http://' : 'https://').$_SERVER['HTTP_HOST'];
  $path = $domain.strtok(dirname($_SERVER["PHP_SELF"])."/",'?');
  $slashes = substr($path,-2);
  if($slashes === '//')
  $path = substr($path,0,strlen($path)-1);
}
?>
<div id="headerShell">
  <div id="header" class="TL absFull">
    <img src="images/ceres_logo.svg" width="120" height="auto" alt="CERES Coin" id="headerLogo" class="TL" />
    <nav id="nav" class="TR">
      <span><a alt="home" href="<?php echo $path; ?>#home">HOME</a></span>
      <span><a alt="about" href="<?php echo $path; ?>#about">ABOUT</a></span>
      <span><a alt="advisors" href="<?php echo $path; ?>#advisors">FOUNDERS &amp; BOARD</a></span>
      <span class="wsub"><a alt="investors">INVESTORS</a>
        <ul class="subnav">
            <li><a alt="pia" href="/pia">What is a PIA?</a></li>
            <li><a alt="investmentprocess" href="/investment-process">Investment Process</a></li>
            <li><a alt="investors" href="http://investor.cerescoin.io">Investor Portal</a></li>
        </ul>
      </span>
      <span><a alt="contact" href="<?php echo $path; ?>#contact">CONTACT US</a></span>
      <span class="navbtn"><a alt="teaser" href="pdf/CERESOnePager_2018.pdf" target="_blank">EXECUTIVE SUMMARY</a></span>
      <span class="navbtn"><a alt="whitepaper" href="pdf/CERESWhitepaper_January2018.pdf" target="_blank">DOWNLOAD WHITEPAPER</a></span>
      <div id="social">
        <a href="https://www.facebook.com/coolmellonceres/" target="_blank" class="icon-facebook"></a>
        <a href="https://www.linkedin.com/company/27115812/" target="_blank" class="icon-linkedin"></a>
        <a href="https://twitter.com/CERESCOIN" target="_blank" class="icon-twitter"></a>
        <a href="https://www.reddit.com/user/CoolMellonSolutions" target="_blank" class="icon-reddit"></a>
      </div>
    </nav>
  </div>
  <div id="openNavBtn" class="TR">
    <div class="row row1 TL"></div>
    <div class="row row2 TL"></div>
    <div class="row row3 TL"></div>
    <div class="row row4 TL"></div>
    <div class="row row5 TR"></div>
  </div>
</div>