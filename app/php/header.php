<div id="headerShell">
<?php
  if(!isset($sectionInfo)){
    $jsSrc = file_get_contents('json/settings.json');
    $jsonData = json_decode($jsSrc, true);
    $sectionInfo = $jsonData['sections'];
    $social = $jsonData['social'];
  }
  if(!isset($isPage)) {
    if(isset($urlPath) and (isset($jsonData['pages'][$urlPath]))) {
      $isPage = true;
      $showNav = $jsonData['pages'][$urlPath]['showNav'];
      $pageSlug = $jsonData['pages'][$urlPath]['id'].' page';
    }	else if($jsonData['promo']['visible']===true){
      $pageSlug .=' promo';
    }
  }
  if(!isset($path)){
    $domain = ((!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "") ? 'http://' : 'https://').$_SERVER['HTTP_HOST'];
    $path = $domain.strtok(dirname($_SERVER["PHP_SELF"])."/",'?');
    $slashes = substr($path,-2);
    if($slashes === '//')
    $path = substr($path,0,strlen($path)-1);
  }
  $root = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);
  $root = 'http://'.$root.'/';

  if ($jsonData['promo']['visible']===true and (!$isPage)){
    echo '<div id="promoWrapper" class="TL">'.$jsonData['promo']['copy'].'</div>';
  }
?>
<div id="header" class="TL absFull">
  <?php
    if($isPage){
      echo '<a href="/"><img src="images/ceres_logo.svg" width="120" height="auto" alt="CERES Coin" id="headerLogo" class="TL"></a>';
    } else {
      echo '<img src="images/ceres_logo.svg" width="120" height="auto" alt="CERES Coin" id="headerLogo" class="TL">';
    }
  ?>
<?php if($showNav) { ?>
  <nav id="nav" class="TR">
  <?php
  foreach($sectionInfo as $value) {
    // if(isset($value['showNav']) and $value['showNav']==true) {
      $activeCheck = ($value['id'] === $pageSlug) ? 'class="active" ' : '';
      $pageCheck = ($isPage) ? $root : '';
      $href = (isset($value['url']) && (strpos($value['url'], '.') !== false)) ? ('href="'.$pageCheck.$value['url'].'" target="_blank"') : ('href="'.$pageCheck.$value['url'].'"');
      $sectCheck = (isset($value['sectionOff']) && ($value['sectionOff'] == true)) ? ' sectoff' : '';
      $navbtn = (isset($value['isButton']) && ($value['isButton'] == true)) ? ' navbtn' : '';
      $subCheck = (isset($value['subnav'])) ? ' wsub' : '';
      echo '<span class="'.$navbtn.$subCheck.$sectCheck.'"><a alt="'.$value['id'].'"'.$activeCheck.' '.$href.'>'.$value['menulabel'].'</a>';
      if(isset($value['subnav'])) {
        $subnav = $value['subnav'];
        $subText = '<ul class="subnav">';
        foreach($subnav as $sub) {
          $subText .= '<li><a alt="'.$sub['id'].'" href="'.$sub['url'].'">'.$sub['menulabel'].'</a></li>';
        }
        $subText .= '</ul></span>';
        echo $subText;
      } else {
        echo '</span>';
      }
    // }
  } 
      
    ?>
    <div id="social">
    <?php foreach($social as $value) { ?>
      <a href="<?php echo $value['link'] ?>" target="_blank" class="icon-<?php echo $value['site'] ?>"></a>
      <?php } ?>
      </div>

    </nav>
      <?php } ?>
      
      </div>
      <?php if($showNav) { ?>
        <div id="openNavBtn" class="TR">
        <div class="row row1 TL"></div>
        <div class="row row2 TL"></div>
        <div class="row row3 TL"></div>
        <div class="row row4 TL"></div>
        <div class="row row5 TR"></div>
        </div>
        <?php } ?>
        </div>
