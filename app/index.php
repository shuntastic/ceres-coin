<?php

require_once('php/Mobile_Detect.php');
$detect = new Mobile_Detect;

$domain = ((!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "") ? 'http://' : 'https://').$_SERVER['HTTP_HOST'];

$path = $domain.strtok(dirname($_SERVER["PHP_SELF"])."/",'?');
$slashes = substr($path,-2);
if($slashes === '//')
$path = substr($path,0,strlen($path)-1);

$jsSrc = file_get_contents('json/settings.json');
$jsonData = json_decode($jsSrc, true);

$url = $jsonData['metadata']['url'];
$title = $jsonData['metadata']['title'];
$desc = $jsonData['metadata']['desc'];
$keywords = $jsonData['metadata']['keywords'];
$thumb = $path.$jsonData['thumb'];
$favicon = $path.$jsonData['favicon'];
$sectionInfo = $jsonData['sections'];
$social = $jsonData['social'];
if(isset($_GET['uri'])){
  $urlPath = $_GET['uri'];
} elseif (isset($jsonData['pages']['home'])) {
  $urlPath = 'home';
}
$showNav = true;
$isPage = false;
$pageSlug = '';
if(isset($urlPath) and (isset($jsonData['pages'][$urlPath]))) {
  $isPage = true;
  $showNav = $jsonData['pages'][$urlPath]['showNav'];
  $pageSlug = $jsonData['pages'][$urlPath]['id'].' page';
}	else if($jsonData['promo']['visible']===true){
  $pageSlug .=' promo';
}
?>

<!DOCTYPE html>
<html class="no-js <?php if( $detect->isMobile()) { if (!$detect->isTablet()) { ?>phone mobile<?php } else { ?>tablet no-phone mobile<?php } } else { ?>desktop no-phone<?php } ?>">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<title><?php echo $title; ?></title>

<meta name="title" content="<?php echo $title; ?>" />
<meta name="description" content="<?php echo $desc; ?>" />

<link rel="apple-touch-icon" sizes="57x57" href="<?php echo $path; ?>faviconsapple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo $path; ?>favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo $path; ?>favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo $path; ?>favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo $path; ?>favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo $path; ?>favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo $path; ?>favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo $path; ?>favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $path; ?>favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo $path; ?>favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo $path; ?>favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo $path; ?>favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo $path; ?>favicon/favicon-16x16.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo $path; ?>favicon/favicon-16x16.png">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">

<meta name="keywords" content="<?php echo $keywords; ?>" />
<link rel="shortcut icon" href="<?php echo $favicon; ?>" type="image/x-icon">
<meta property="og:title" content="<?php echo $title; ?>" />
<meta property="og:type" content="website" />
<meta property="og:image" content="<?php echo $thumb; ?>" />
<meta property="og:description" content="<?php echo $desc; ?>" />
<meta property="og:site_name" content="<?php echo $path; ?>"/>

<meta itemprop="name" content="<?php echo $title; ?>">
<meta itemprop="description" content="<?php echo $descr; ?>">
<meta itemprop="image" content="<?php echo $thumb; ?>">
<link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,800|Roboto:400,500,700" rel="stylesheet">
<link rel="stylesheet" href="https://use.typekit.net/ngn3jtg.css">

<!-- <link rel="image_src" type="image/jpeg" href="./img/share.jpg" /> -->
<link rel="stylesheet" href="<?php echo $path; ?>css/main.css">

<!-- build:remove -->
<script src="//localhost:35729/livereload.js"></script>
<!-- /build -->
<script src='https://www.google.com/recaptcha/api.js'></script>
</head>

<body class="<?php echo $pageSlug; ?>" style="display:none;">
<?php
// ADDS OPTIONAL STYLES AND ANIMATION TO OVERALL CONTENT AREA
$props = $jsonData['properties'];
$style = ($props['style']) ? ' style="'.$props['style'].'"' : '';
$class = ($props['animation']) ? 'class="wow '.$props['animation']['type'].'"' : 'class=""';

//applies animation options
$animation_duration = ($props['animation']['duration']) ? 'data-wow-duration="'.$props['animation']['duration'].'"' : '';
$animation_delay = ($props['animation']['delay']) ? 'data-wow-delay="'.$props['animation']['delay'].'"' : '';
$animation_offset = ($props['animation']['offset']) ? 'data-wow-offset="'.$props['animation']['offset'].'"' : '';
$animation_iteration = ($props['animation']['iteration']) ? 'data-wow-iteration="'.$props['animation']['iteration'].'"' : '';
?>
<div id="main" <?php echo $style.' '.$class.' '.$animation_duration.' '.$animation_delay.' '.$animation_offset.' '.$animation_iteration;?>>

  <?php 
      include('php/header.php');
    // if($isPage == true){
    //   include('php/header_static_page.php');
    // } else {
    //   include('php/header_static.php');
    // }
  ?>
        <div id="sections">
        <div id="sectionWrapper" class="">
        
        <?php
        if(isset($urlPath) and (isset($jsonData['pages'][$urlPath]))) {
          $file = $jsonData['pages'][$urlPath]['file'];
          if (strpos($file, '.php') !== false) {
            echo '<section id="'.$jsonData['pages'][$urlPath]['id'].'">';
            include($file);
            echo '</section>';
          } else {
            echo '<section id="'.$jsonData['pages'][$urlPath]['id'].'">'.file_get_contents($file).'</section>';
          }
        } elseif(isset($urlPath)) { 
          // SERVE 404 MESSAGE
          ?>
          <section id="notFound">';
            <div id="homeBG" class="TL absFull"></div>
            <div class="subsect sect">
                <h1>Page Not Found</h1>
                <p>The page you were looking for could not be found. <a href="\#home">Return Home</a>.</p>
            </div>
        
          </section>;
       
        <?php
        } else {
          $val = '';
          $newSection = '';
          $mainctr = 1;
          $ctr = 1;
          foreach($sectionInfo as $value) {
            if(!$value['sectionOff']){
            // if(!$value['page'] && !$value['sectionOff']){

              $newSection .= '<section id="'.$value['id'].'" class="sect section'.$mainctr.' scrollspy" data-hash="'.$mainctr.'">';
  
              if(isset($value['templatefile'])){
                // $newSection .= '<div id="sect'.$ctr.'">';
                $file = $value['templatefile'];
                $newSection .= file_get_contents($file);
                // $newSection .= '</div>';
                $ctr++;
              } else {
                
                foreach($value['subsections'] as $section => $item) {
                  //applies optional styles and animation
                  $sect = $item;
                  // $sect = $value[$section];
                  $canvid = (( $detect->isMobile()) and ($sect['type'] == 'cover')) ? ' canvasvideo' : '';
                  $style = ($sect['properties']['style']) ? ' style="'.$sect['properties']['style'].'"' : '';
                  $class = ($sect['properties']['animation']) ? 'class="'.$sect['type'].' wow '.$sect['properties']['animation']['type'].$canvid.'"' : 'class="'.$sect['type'].$canvid.'"';
                  
                  //applies animation options
                  $animation_duration = ($sect['properties']['animation']['duration']) ? 'data-wow-duration="'.$sect['properties']['animation']['duration'].'"' : '';
                  $animation_delay = ($sect['properties']['animation']['delay']) ? 'data-wow-delay="'.$sect['properties']['animation']['delay'].'"' : '';
                  $animation_offset = ($sect['properties']['animation']['offset']) ? 'data-wow-offset="'.$sect['properties']['animation']['offset'].'"' : '';
                  $animation_iteration = ($sect['properties']['animation']['iteration']) ? 'data-wow-iteration="'.$sect['properties']['animation']['iteration'].'"' : '';
                  $idname = ($sect['id']) ? $sect['id'] : 'sect'.$ctr;
                  //start building section
                  $newSection .= '<div id="sect'.$ctr.'" '.$class.' '.$animation_duration.' '.$animation_delay.' '.$animation_offset.' '.$animation_iteration.'>';
                  // $newSection .= '<div id="'.$idname.'" '.$class.' '.$animation_duration.' '.$animation_delay.' '.$animation_offset.' '.$animation_iteration.'>';
                  
                  //title
                  if(($sect['title'])&&(($sect['type']!=='cta')&&($sect['type']!=='cover')))
                  $newSection .= '<h2>'.$sect['title'].'</h2>';
                  
                  switch($sect['type']) {
                    
                    case "blockquote" :
                    $newSection .='<blockquote'.$style.'>'.$sect['copy'].'</blockquote>';
                    break;
                    
                    case "text" :
                    // $newSection .='<div'.$style.'>'.$sect['copy'].'</div>';
                    $newSection .= $sect['copy'];
                    
                    break;
                    
                    case "image":
                    $newSection .='<div class="img"'.$style.'>';
                    foreach($sect['imageList'] as $image) {
                      $newSection .='<img src="'.$path.$image.'" /><br />';
                    }
                    $newSection .='</div>';
                    
                    break;
                    
                    case "gallery":
                    $newSection .='<div class="gallery">';
                    foreach($sect['slideList'] as $slide) {
                      $newSection .='<div class="content-slide" '.$style.'>'.$slide.'</div>';
                    }
                    $newSection .='</div>';
                    if($sect['navList']) {
                      $newSection .='<div class="gallery-nav TL HC">';
                      $gctr = 0;
                      foreach($sect['navList'] as $item) {
                        $curr = ($gctr == 0) ? 'class="current" ' : '';
                        $newSection .='<a data-galref="'.$gctr.'" '.$curr.'href="javascript:void(0);">'.$item.'</a>';
                        $gctr++;
                      }
                      $newSection .='</div>';
                    }
                    
                    break;
                    
                    case "video":
                    $vWidth = ($sect['source']['width'])?' width="'.$sect['source']['width'].'" ':'';
                    $vHeight = ($sect['source']['height'])?' height="'.$sect['source']['height'].'" ':'';
                    $poster = ($sect['source']['posterimage'])?' poster="'.$path.$sect['source']['posterimage'].'" ':'';
                    $loop = ($sect['source']['loop'])?'loop ':'';
                    $autoplay = ($sect['source']['autoplay'])?'controls autoplay':'controls';
                    $newSection .='<div class="videoWrapper">';
                    
                    if($sect['source']['mp4']){
                      $newSection .= '<video'.$poster.$vWidth.$vHeight.' '.$loop.$autoplay.'><source src="'.$path.$sect['source']['webm'].'" type="video/webm"/><source src="'.$path.$sect['source']['mp4'].'" type="video/mp4"/></video><a href="#" id="playBtn"></a>';
                      
                    } else {
                      $newSection .= '<iframe src="'.$sect['source']['vimeo'].'"'.$vWidth.$vHeight.'frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
                    }
                    $newSection .='</div>';
                    
                    break;
                    
                    case "cover":
                    $poster = ($sect['source']['posterimage'])?' poster="'.$path.$sect['source']['posterimage'].'" ':'';
                    $posterStyle = ($sect['source']['posterimage'])?' style="background:url('.$path.$sect['source']['posterimage'].') center center no-repeat;" ':'';
                    
                    if($sect['source']['mp4']){
                      $newSection .= '<article '.$posterStyle.'class="videoArea"><video'.$poster.'  preload autoplay muted loop autoplay muted webkit-playsinline><source src="'.$path.$sect['source']['webm'].'" type="video/webm"/><source src="'.$path.$sect['source']['mp4'].'" type="video/mp4"/></video></article>';
                      
                    } else {
                      $newSection .= '<article '.$posterStyle.'class="videoArea"></article>';
                    }
                    $newSection .='<article class="contentArea"><div class="caption wow fadeInDown" data-wow-duration="1.5s"><h1>'.$sect['title'].'</h1><p>'.$sect['copy'].'</p></div></article>';
                    
                    break;
                    
                    case "cta":
                    $target = ($sect['target'])?' target="'.$sect['target'].'"':'';
                    $newSection .='<div class="box"><h3 '.$style.'><a href="'.$sect['url'].'"'.$target.'>'.$sect['title'].'</a></h3></div>';
                    // $newSection .='<div class="box"><svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%"><line class="top" x1="0" y1="0" x2="420" y2="0"/><line class="left" x1="0" y1="50" x2="0" y2="-920"/><line class="bottom" x1="500" y1="50" x2="-600" y2="50"/><line class="right" x1="420" y1="0" x2="420" y2="1380"/></svg><h3 '.$style.'><a href="'.$sect['url'].'"'.$target.'>'.$sect['title'].'</a></h3></div>';
                    // $newSection .='<div class="cta"><a href="'.$sect['url'].'"'.$target.'>'.$sect['title'].'</a></div>';
                    
                    break;
                    
                  };
                  $newSection .='</div>';
                  $ctr++;
                }
              }
              $newSection .='</section>';
              $mainctr++;
            }
          }
          echo $newSection;
        }
        ?>
        </div>
        </div>
      </div>
      <footer>
        <p class="copyright"><?php echo $jsonData['copyright']; ?></p>
      </footer>
        <script type="text/javascript">
        var SITE = SITE || {};
          SITE.settings = <?= json_encode($jsonData); ?>;
          var isPage = <?php echo ((isset($isPage) && $isPage == true) ? 'true' : 'false' ); ?>;
          </script>
          
          <!--[if gt IE 9]><!-->
          <!-- build:js /js/main.js-->
          <script type="text/javascript" data-main="<?php echo $path; ?>js/main" src="<?php echo $path; ?>js/packages/requirejs/require.js"></script>
          <!-- /build-->
          <!--<![endif]-->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-115447527-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-115447527-1');
</script>
</body>
          </html>
          