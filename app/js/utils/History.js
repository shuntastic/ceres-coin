/*jslint browser:true */
/*jshint undef: false */
/*globals define, $, _, SITE */
;
(function (root, factory) {
	"use strict";
	root.utils = root.utils || {};

	if (typeof define === 'function' && define.amd) {
		define(
			[
				'jquery',
				'lib/modernizr',
				'underscore'
			],
			function () {
				var o = factory($);
				return (root.utils.history = o);
			}
		);
	} else {
		var o = factory($);
		root.utils.history = o;
	}
}(window.SITE = window.SITE || {}, function () {
	var that,
		enabled = true,
		lastState = '';

	function History() {
		that = this;
	}

	function init() {
		if (!Modernizr.history) {
			enabled = false;
			return;
		}
		$(window).on('popstate', onPopState);
	}

	function onPopState() {
		var split = (unescape(window.location.pathname)).split('/'),
			nextState = (split[split.length - 1].indexOf('index.php') != -1 ||
				split[split.length - 1].indexOf('.php') == -1) ? 'home' : split[split.length - 1].split('.php')[0];

		if (nextState != lastState) {
			SITE.app.sectionControl.changeWithoutPush(nextState);
			lastState = nextState;
		}

		return false;
	}

	function pushState(value) {
		if (!enabled || value == lastState) {
			return;
		}
		history.pushState({
				path: SITE.settings.sections[value].url
			},
			SITE.settings.sections[value].title,
			SITE.settings.sections[value].url
		);

		lastState = value;
	}


	History.prototype.init = init;
	History.prototype.pushState = pushState;
	return new History();
}));