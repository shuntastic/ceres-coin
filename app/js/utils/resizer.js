/*jslint browser:true */
/*globals define, $, _, SITE */
(function (root, factory) {
	"use strict";
	root.utils = root.utils || {};

	if (typeof define === 'function' && define.amd) {
		define(
			[
				'jquery'
			],
			function () {
				var o = factory($);
				if (!window.resizer) {
					window.resizer = o;
				}
				return (root.utils.resizer = o);
			}
		);
	} else {
		var o = factory($);
		if (!window.resizer) {
			window.resizer = o;
		}
		root.utils.resizer = o;
	}
}(window.SITE = window.SITE || {}, function () {
	var targets = null,
		listeners = null,
		active = false,
		animRequest = 0;

	function Resizer () {
	}

	/**
	 * Initialize the resizer.
	 * @param {object} options Optional properties
	 *                         - targets: array of targets [{ func: function, label: 'unique id' }, ...]
	 *                         - immediateRender: force an immediate dispatch
	 */
	function init(options) {
		active = true;
		$(window).on('resize orientationchange', handleResize);
		if (options) {
			options.targets && addTarget(options.targets);
			options.immediateRender && handleResize();
		}
	}

	/**
	 * Force resizer to dispatch the sizes objects even if no resize has occured
	 */
	function force() {
		requestDispatch();
	}

	/**
	 * Stop dispatching data
	 */
	function stop() {
		active = false;
	}


	/**
	 * Start dispatching data
	 */
	function start() {
		active = true;
	}

	/**
	 * Add a target to retrieve the dimensions of from a window resize. Can also accept
	 * an array of target objects.
	 * @param {(Array|Object)} value - { label: 'unique id', target: jquery object }
	 */
	function addTarget(value) {
		if (Array.isArray(value)) {
			value.forEach(function (item) { addTarget(item); });
			return;
		}

		!targets && (targets = {});

		typeof value === 'object' && !targets[value.label] && (targets[value.label] = value.target);
	}

	/**
	 * Remove a target object from the size report.
	 * @param {sring} label - label for the target to remove
	 */
	function removeTarget(label) {
		targets[label] && delete(targets[label]);
	}

	/**
	 * Add a listener for resize events.
	 * @param {Object} value - { label: 'unique id', target: function }
	 */
	function addListener(value) {
		!listeners && (listeners = {});

		if (listeners[value.label]) {
			console.log('Resizer: listener label already exists', value.label);
			return;
		}

		listeners[value.label] = value.func;
		value.immediateRender && handleResize();
	}

	/**
	 * Remove a listener from receiving resize notices
	 * @param {sring} label - label for the target to remove
	 */
	function removeListener(label) {
		listeners[label] && delete(listeners[label]);
	}


	function handleResize() {
		requestDispatch();
	}

	function requestDispatch() {
		if (animRequest || !active || !listeners || !targets) {
			return;
		}
		animRequest = window.requestAnimationFrame(dispatch);
	}

	function dispatch() {
		animRequest = 0;
		var obj = {},
			keys = Object.keys(targets),
			i = keys.length;

		while(i--) {
			var key = keys[i],
				target = targets[key];
			try {
				obj[key] = {
					width: target.width(),
					height: target.height(),
					minWidth: target.css('min-width'),
					minHeight: target.css('min-height'),
					maxWidth: target.css('max-width'),
					maxHeight: target.css('max-height')
				};
			}

            catch (e) {

				obj[key] = {
					width: target.width(),
					height: target.height()
				};
            }
		};
		keys = Object.keys(listeners),
			i = keys.length;
		while(i--) {
			listeners[keys[i]](obj);
		};
	}

	Resizer.prototype.init = init;
	Resizer.prototype.stop = stop;
	Resizer.prototype.start = start;
	Resizer.prototype.force = force;
	Resizer.prototype.addTarget = addTarget;
	Resizer.prototype.addListener = addListener;
	Resizer.prototype.removeTarget = removeTarget;
	Resizer.prototype.removeListener = removeListener;
	return new Resizer();
}));
