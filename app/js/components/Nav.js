/*jslint browser:true */
/*jshint undef:false */
/*globals define, $, SITE, _, TweenLite, TweenMax, Expo, TimelineMax, Linear */


;(function (root, factory) {
	"use strict";
	root.app = root.app || {};

	if (typeof define === 'function' && define.amd) {
		define(
			[

				'jquery',
				'underscore',
				'iscroll',
				'utils/Resizer',
				'app/SectionControl',
				'app/components/VideoDiv',
				'utils/Localizr',
				'utils/Preloader'
			],
			function () {
				var o = factory($);
				return (root.app.nav = o);
			}
		);
	} else {
		var o = factory($);
		root.app.nav = o;
	}
}(window.SITE = window.SITE || {}, function () {
	var IScroll = require('iscroll');
	var me,
		el,
		bg,
		enterTL,
		exitTL,
		menuH = 0,
		_isOpen = false,
		sectionChangeFlag = false,
		logoShow = true,
		bgEnabled = true

	;

	function Nav() {
		me = this;
	}

	function init(params) {
		data = SITE.settings.nav;

		bgEnabled = data.background.enabled;
		if (bgEnabled && sniffer.device === 'mobile') {
			if (!sniffer.isiOS || (sniffer.isiOS && sniffer.v < 10)) {
				bgEnabled = false;
			}
		}

		layout();
		initBtns();
		el = $('#mainMenu');

		enterTL = (new TimelineMax({
			paused: true,
			immediateRender: true,
			onStart: onEnterStart,
			onComplete: onEnterComplete,
			onReverseComplete: onExit
		}))
			.fromTo('#mainMenuBgShell', 0.75, { width:'0%' }, { width: '100%', ease: Power2.easeOut })
			.staggerFromTo(
				$('#mainMenu .mainMenuBtn'), 0.6,
				{
					y: 15,
					opacity: 0
				},
				{
					y: 0,
					ease: Expo.easeOut,
					opacity: 1
				},
				0.07,
				0.35
			);

		resizer.addListener({ func: onResize, label: 'nav'});
	}

	function layout() {
		// var bgParams = data.background,
		// 	bgImg = SITE.utils.preloader.getLoader('videosBackground');
		$(SITE.utils.localizr.template(
			SITE.utils.preloader.getLoader('nav-template').getData(),
			{
				menuList: SITE.settings.nav.menu,
				backgroundVideoEnabled: bgEnabled
			})
		  ).appendTo('#sections');
		$('.navBtn[rel='+SITE.settings.curSection+']').addClass('selected');
		SITE.utils.localizr.parseObjectList(SITE.settings.nav.items);
		// $.extend(
		// 	bgParams,
		// 	{
		// 		enabled: bgEnabled,
		// 		video: bgParams.video,
		// 		image: bgImg.img.src,
		// 		parent: $('#mainMenuBg'),
		// 		preload: true,
		// 		muted: true,
		// 		width: bgImg.img.width,
		// 		height: bgImg.img.height,
		// 		autoplay: false
		// 	});

		// bg = new SITE.app.VideoDiv( bgParams );
		// bg.load();

	}

	function initBtns() {
		$('.mainMenuBtn').click(onMainMenuBtnClick);
	}

	function enter() {
		_isOpen = true;
		resizer.force();
		el.addClass('open');
		enterTL.timeScale(1).restart();
        $('#openNavBtn, #shellInner').removeClass('close').addClass('open');
        if($('#header').hasClass('hidden')){
        	logoShow = false;
        }
        SITE.app.framework.showLogo();
		$(document).on('touchstart mousedown', onOutsideClick);
	}

	function onEnterStart() {
		el.css('display', 'block');
		if (!menuH) {
			menuH = $('#mainMenuBtns').height();
		}
		resizer.force();
	}

	function onEnterComplete() {
	}

	function exit(cueNext) {
		if($('#header').hasClass('hidden')){
			SITE.app.framework.hideLogo();
		}
		el.removeClass('open');
        $(document).off('touchstart mousedown', onOutsideClick);
		if (cueNext) {
			if (!_isOpen) {
				SITE.utils.sequencer.next();
				return;
			}
			sectionChangeFlag = true;
		}
		_isOpen = false;
		enterTL.timeScale(1.8).reverse();
        $('#openNavBtn, #shellInner').removeClass('open').addClass('close');
//		exitTL.timeScale(1).restart();
	}

	function onExit() {
		el.css('display', 'none');
		// bg.stop();

		if (sectionChangeFlag) {
			sectionChangeFlag = false;
			SITE.utils.sequencer.next();
		}
	}

	function toggle() {
		if (_isOpen) {
			exit();
		} else {
			enter();
		}
	}

	function onMainMenuBtnClick() {
		var thisBtn = $(this);
		if (thisBtn.attr('type') == 'section') {
//			trackEvent('Main Menu', 'Site Section Clicked', thisBtn.attr('trackingid'));
			SITE.app.sectionControl.change(thisBtn.attr('rel'));
			return false;
		}
//		trackEvent('Main Menu', 'Exit Link Clicked', thisBtn.attr('trackingid'));

		return;
	}

	function onResize(sizes) {
		var wOffset = $('#header').width() + 155;
//		$('#mainMenuBgShell').css('max-width',wOffset+'px');
		// bg.resize(sizes.shell.width, sizes.shell.height);
		var menuMT = parseInt($('#mainMenu').css('margin-top'));
		var menuB = menuMT + $('#mainMenu').height();
		TweenLite.set('#mainMenu', { scale: Math.min(1,  (sizes.shell.height - menuMT - 40) / $('#mainMenu').height() )});

	}

	function isOpen() {
		return _isOpen;
	}

	function update() {
		$('.navBtn').removeClass('selected');
		$('.navBtn[rel='+SITE.settings.curSection+']').addClass('selected visited');
	}

	function onOutsideClick(e) {
	
        if (e && ($(e.target).closest('#mainMenu').length || $(e.target).closest('#openNavBtn').length || $(e.target).closest('.logoBtn').length || $(e.target).closest('.mobile .socialBtn').length || $(e.target).closest('.mobile #sounds').length || $(e.target).closest('.mobile #getTickets').length || $(e.target).closest('.mobile #legalBtn').length)) {
            return;
        }
		exit();
	}

	function setTop(val) {
		$('#mainMenu').css('margin-top', val);
	}

	Nav.prototype.init = init;
	Nav.prototype.enter = enter;
	Nav.prototype.exit = exit;
	Nav.prototype.toggle = toggle;
	Nav.prototype.isOpen = isOpen;
	Nav.prototype.update = update;
	Nav.prototype.setTop = setTop;
	return new Nav();
}));
