/*jslint browser:true */
/*globals define, $, SITE, _, Expo, TimelineMax */
/**
 *
 */
;(function (root, factory) {
    "use strict";
    root.app = root.app || {};
	var o = factory($);
	root.app.VideoPlayerYTPlaylist = o;
}(window.SITE = window.SITE || {}, function () {

    function VideoPlayerYTPlaylist(params) {
		this.parent = params.parent;
		this.videoData = params.videoData;
		this.player = null;
		this.onReady = params.onReady;
		this.load();
	}
	VideoPlayerYTPlaylist.prototype = {

		load: function () {
			this.parent.empty().append('<div id="ytPlayer"></div>');
			if (this.player != null) {
				this.player = null;
			}
			if (this.player === null) {
				this.player = new YT.Player(
					'ytPlayer',
					{
						height: '100%',
						width: '100%',
						'background-color': '#000',
						allowtransparency: 'true',
						playerVars: {
							autohide: 1,
							autoplay: 1,
							controls: 1,
							enablejsapi: 1,
							modestbranding: 1,
							rel: 0,
							showinfo: 1,
							wmode: 'transparent',
							listType: 'playlist',
							list: this.videoData.videoId

						},
						events: {
							'onReady': $.proxy(this.onPlayerReady, this)
						}
					}
				);
			}
		},
		
		play: function () {
			if (this.player && this.player.playVideo) {
				this.player.playVideo();
			}
		},
		
		pause: function() {
			if (this.player && this.player.pauseVideo) {
				this.player.pauseVideo();
			}	
		},

		onPlayerReady: function () {
			this.onReady();
		},

		onStateChange: function(e) {
			switch (e.data) {
				case 3: // buffering
				case 1: // playing
			}
		},
		
		destroy: function() {
			this.parent.empty();
			if (this.player != null) {
				this.player = null;
			}
		},
		
		change: function(newData) {
			this.videoData = newData;
			this.load();
		}
	}
	
    return VideoPlayerYTPlaylist;
}));
