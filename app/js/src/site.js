// (function(window) {
(function(root, factory) {
  'use strict';
  root.sections = root.sections || {};

  if (typeof define === 'function' && define.amd) {
    define(
      [
        'jquery',
        'utils/sniffer',
        'packages/greensock/TweenMax.min',
        'packages/greensock/TimelineMax.min',
        'packages/greensock/easing/EasePack.min',
        'packages/Sequencer',
        'utils/resizer',
        'components/VideoPlayerYT',
        'components/VideoPlayerYTPlaylist',
        'components/VideoDiv',
        'components/VideoPlayerMP4'
      ],
      function() {
        var o = factory($);
        return (root.sections.home = o);
      }
    );
  } else {
    var o = factory($);
    root.sections.home = o;
  }
})((window.SITE = window.SITE || {}), function() {
  // standard global variables
  var home,
    me,
    bg,
    ee,
    wow,
    // slideLoop = false,
    // slideNum = 3,
    // currentSlide = 1,
    myScroll,
    inited = false,
    canvasVideo,
    urlParams = {},
    mainSect,
    sliderTL,
    lastId,
    topMenu,
    topMenuHeight,
    menuItems,
    scrollItems,
    data,
    isOpen = false,
    curVideo = -1,
    videoData,
    player = null;

  function Home() {
    me = this;
  }

  function init() {
    if (inited) return;
    inited = true;

    mainSect = $('#sectionHolder');

    $('#openNavBtn').click(function() {
      var active = $(this).hasClass('open');
      if (active) {
        $('#openNavBtn,#nav,#social').removeClass('open');
      } else {
        $('#openNavBtn,#nav,#social').addClass('open');
      }
    });

    // $('.cover article video').get().forEach(makeVideoPlayableInline);
    wow = new WOW({
      offset: 100,
      callback: function(box) {
        /**/
      }
      // $('#main').css('display','inherit');
    });
    $('.gallery .gallery')
      .on('init', function(event, slick) {
        /**/
      })
      .on('afterChange', function(event, slick, currentSlide, nextSlide) {
        // console.log(slick, currentSlide, nextSlide);
        $('.gallery-nav a').removeClass('current');
        $('.gallery-nav a[data-galref="' + currentSlide + '"]').addClass('current');
      })
      .slick({
        nextArrow: '<a class="slick-next icon-arrow TR"></a>',
        prevArrow: '<a class="slick-prev icon-arrow TL"></a>',
        fade: true,
        lazyLoad: 'progressive'
      })
      .slick('slickGoTo', 0, false);

    // var overviewInt = setInterval(function() {
    //   $('.gallery .gallery').slick('slickNext');
    // }, 4200);

    // $('.gallery-nav a').click(function() {
    //   var newSlide = parseInt($(this).attr('data-galref'));
    //   $('.gallery .gallery').slick('slickGoTo', newSlide);
    //   $('.gallery-nav a').removeClass('current');
    //   $(this).addClass('current');
    // });
    wow.init();

    var bgParams = {};
    // bgParams.video =
    //   sniffer.device === 'mobile' || sniffer.device === 'tablet' ? bgParams.video_mobile : bgParams.video;

    $.extend(bgParams, {
      enabled: true,
      video: SITE.settings.sections[0].intro.videoBg,
      image: SITE.settings.sections[0].intro.bgImg,
      parent: $('#homeBG'),
      preload: true,
      muted: true,
      width: 1920,
      height: 1080,
      // width: 1920,1153 × 867
      // height: 1080,
      autoplay: false
    });

    bg = new SITE.app.VideoDiv(bgParams);
    bg.load();
    bg.setSpeed(1.7);
    bg.play();
    onResize(
      {
        window:
          {
            width: window.innerWidth, height: window.innerHeight
          }
        }
    );

    $('#nav a[alt="investors"]').attr('href',SITE.settings.sections[3].url).attr('target','_blank');
    if(!isPage){
      initScrollNav();
    }

    $('#playVideo').click(function(e) {
      setVideo(0);
      loadVideo();
      e.preventDefault();
    });

    $('#videoOverlayClose').click(function(e) {
      closePlayer();
      e.preventDefault();
    });

    $('#contactBtn').click(function(e) {
      $('body').addClass('showoverlay');
      // window.location = 'mailto:info@coolmellon.com';
      e.preventDefault();
    });
    $('#contactForm .closeBtn').click(function(e) {
      $('body').removeClass('showoverlay');
      e.preventDefault();
    });
    $('#contactForm').submit(function(e) {
      var form = $(this);
      var formMessages = $('#formStatus');

      var data = {
        name: form.find('input[name="name"]').val(),
        phone: $(this)
          .find('input[name="phone"]')
          .val(),
        email: form.find('input[name="email"]').val(),
        query: $(this)
          .find('textarea[name="query"]')
          .val()
      };
      $.post(window.location.origin + '/php/mailer.php', data)
        .done(function(response) {
          console.log('done:', response);
          formMessages.removeClass('error');
          formMessages.addClass('success');
          formMessages.text('Thank you for your message, we will follow up shortly.');

          // Clear the form.
          $('#name').val('');
          $('#email').val('');
          $('#phone').val('');
          $('#query').val('');
        })
        .fail(function(response) {
          console.log('fail:', response);
          $(formMessages).removeClass('success');
          $(formMessages).addClass('error');

          if (response.responseText !== '') {
            $(formMessages).text('Oops! An error occured and your message could not be sent.');
            console.log('fail response:', data.responseText);
            // $(formMessages).text(data.responseText);
          } else {
            $(formMessages).text('Oops! An error occured and your message could not be sent.');
          }
        });

      e.preventDefault();
    });

    // $('.videoArea video').get().forEach(makeVideoPlayableInline);
    resizer.addListener({ func: onResize, label: 'home' });
  }

  function closeMenu() {
    if ($('#openNavBtn').hasClass('open')) $('#openNavBtn,#nav,#social').removeClass('open');
  }

  function onResize(sizes) {
    bg.resize(sizes.window.width, sizes.window.height);

    // $('.cover article video').resize(sizes.window.width, sizes.window.height);
    if (myScroll) {
      refreshScroll();
    }
  }
  function initScrollNav() {
    topMenu = $('#header');
    topMenuHeight = topMenu.outerHeight();
    // All list items
    menuItems = topMenu.find('a').not('.sectoff a,.navbtn a, [target="_blank"], [alt="investors"], #social a, .subnav a');
    // console.log(menuItems);
    // menuItems.concat($('#footerNav a'));
    // Anchors corresponding to menu items
    scrollItems = menuItems.map(function() {
      // if (
      //   $(this)
      //     .attr('href')
      //     .indexOf('/') == 0
      // ) {
      //   return false;
      // }
      var item = $($(this).attr('href'));
      if (item.length) {
        return item;
      }
    });
    window.location.hash = window.location.hash.length > 0 ? window.location.hash.replace('/', '') : '';

    // Bind click handler to menu items
    // so we can get a fancy scroll animation
    menuItems.click(function(e) {
      var href = $(this).attr('href'),
        offsetTop = href === '#' ? 0 : $(href).offset().top - topMenuHeight + 1 + (href === '#contact' ? 0 : 0);
      $('html, body')
        .stop()
        .animate(
          {
            scrollTop: offsetTop
          },
          460
        );
      closeMenu();
      e.preventDefault();
    });

    // ScrollNav handler
    if ($('#nav').length > 0) {
      $(window).scroll(function() {
        console.log('scroll');
        // Get container scroll position
        var fromTop = $(this).scrollTop() + topMenuHeight;
        // Get id of current scroll item
        var cur = scrollItems.map(function() {
          if ($(this).offset().top < fromTop) return this;
        });
        // Get the id of the current element

        cur = cur[cur.length - 1];
        var id = cur && cur.length ? cur[0].id : '';

        if (lastId !== id) {
          lastId = id;
          // Set/remove active class
          menuItems
            .parent()
            .removeClass('active')
            .end()
            .filter("[href='#" + id + "']")
            .parent()
            .addClass('active');
          document.getElementsByTagName("body")[0].className = '';
          document.getElementsByTagName("body")[0].className = id;
          window.location.hash = '/' + id;
        }
      });
    }

  }

  function setVideo(num) {
    // curVideo = num;
    // if (curVideo === 'intro') {
    //   $('body').addClass('intro');
    //   videoData = SITE.settings.sections.home.intro;
    // } else {
    //   videoData = data.parsedLinks[curVideo];
    // }
    videoData = SITE.settings.sections[0].intro;
  }

  function loadVideo() {
    // musicWasMuted = $('body').hasClass('muted');
    // autoMute();

    if (player) {
      player.destroy();
    }
    switch (videoData.type) {
      case 'youtube':
      case 'youtubePlaylist':
        player = new SITE.app.VideoPlayerYT({
          parent: $('#videoShell'),
          videoData: videoData,
          onReady: onPlayerReady,
          onStateChange: onStateChange
        });
        break;

      case 'mp4':
        player = new SITE.app.VideoPlayerMP4({
          parent: $('#videoShell'),
          videoData: videoData,
          onReady: onPlayerReady,
          onComplete: onPlayerComplete
        });
        break;

      case 'tumblr':
        player = new SITE.app.VideoPlayerTumblr({
          parent: $('#videoShell'),
          videoData: videoData,
          onReady: onPlayerReady,
          onComplete: onPlayerComplete
        });
        break;
    }
    if (sniffer.device === 'mobile' || sniffer.device === 'tablet') {
      setTimeout(function() {
        $('#videoCurtain').addClass('hidden');
      }, 500);
    }
  }

  function playVideo() {
    if (!isOpen || sniffer.device !== 'desktop') {
      return;
    }
    player.play();
  }

  function onStateChange(state) {
    if (curVideo === 'intro' && state === 0) {
      // console.log('player complete');
      closePlayer();
    }
  }

  // function videoMenuBtnClick(evt) {
  //   var ref = Number(evt.currentTarget.id.replace('vid', ''));
  //   setVideo(ref);
  //   loadVideo();
  // }

  function closePlayer() {
    // console.log('closePlayer');
    isOpen = false;
    if (player) {
      player.pause();
      if (sniffer.device !== 'desktop') {
        player.destroy();
      }
    }
    $('body')
      .removeClass('intro')
      .removeClass('playeropen');
    $('#videoOverlay, #videoOverlayWrapper').css('display', 'none');

    return false;
  }

  function closeMobilePlayer() {
    if (player) {
      player.pause();
      if (sniffer.device !== 'desktop') {
        player.destroy();
      }
    }
    isOpen = false;
    $('body').removeClass('playeropen');
    return false;
  }

  function onPlayerOpen() {
    isOpen = true;
    playVideo();
  }
  function onPlayerClosed() {
    isOpen = false;
    player.destroy();
  }
  function onPlayerComplete() {
    // if (curVideo === 'intro') {
    closePlayer();
    // }
  }
  /**
   * overlay player
   */

  function initPlayer() {}

  function openPlayer() {
    // if (sniffer.device === 'mobile') {
    $('#videoOverlay, #videoOverlayWrapper').css('display', 'block');
    $('body').addClass('playeropen');
    // }

    isOpen = true;
    playVideo();
    // playerTL.restart();
  }

  // function closePlayer() {
  //   if (!musicWasMuted) {
  //     // autoUnmute();
  //   }
  //   $('#videoCurtain').removeClass('hidden');
  //   player.pause();
  //   player.destroy();

  //   isOpen = false;
  //   playerTL.reverse();
  // }

  function onPlayerReady() {
    if (isOpen) {
      // setTimeout(function() {
      //   $('#videoCurtain').addClass('hidden');
      // }, 500);
      playVideo();
    } else {
      openPlayer();
    }
  }

  function pause() {
    if (player && isOpen) {
      player.pause();
    }
  }

  function enter() {


  }

  window.Home = Home;
  window.urlParams = urlParams;
  Home.prototype.init = init;
  Home.prototype.enter = enter;
  return new Home();
});
