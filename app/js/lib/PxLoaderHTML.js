/*globals THREE */
function PxLoaderHTML(url, tags, priority) {
    var self = this,
        loader = null;

    // used by the loader to categorize and prioritize
	this.url = url;
    this.tags = tags;
    this.priority = priority;

	this.ldr = null;
	this.data;

    // called by PxLoader to trigger download
    this.start = function(pxLoader) {
        loader = pxLoader;
		self.ldr = $.get(self.url, self.onPageLoaded).fail(self.onTimeout);
        // start downloading
    };

	this.onPageLoaded = function(data) {
		self.data = data;
        loader.onLoad(self);
	};

    // called by PxLoader to check status of image (fallback in case
    // the event listeners are not triggered).
    this.checkStatus = function() {
        // report any status changes to the loader
        // no need to do anything if nothing has changed
    };

    // called by PxLoader when it is no longer waiting
    this.onTimeout = function() {
		loader.onTimeout(self);
    };

    // returns a name for the resource that can be used in logging
    this.getName = function() {
        return url;
    };

	this.getData = function() {
		return self.data;
	};

}
