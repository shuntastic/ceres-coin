// $(document).ready(init);
var settings = {
  main: { menu: [{ link: 'home', label: 'HOME' }] }
};
var activeSections = [],
  section = '',
  baseURL = '',
  isMobile = false;

requirejs.config({
  paths: {
    jquery: 'packages/jquery-1.10.2.min',
    // iscroll: 'lib/iscroll',
    pxloader: 'packages/PxLoader/PxLoader',
    pxloaderimage: 'packages/PxLoader/PxLoaderImage'
  },
  map: {
    '*': {
      jQuery: 'jquery',
      iscroll: 'lib/iscroll',
      Resizer: 'utils/resizer',
      underscore: 'packages/underscore-min',
      Categorizr: 'packages/Categorizr'
      // 'greensock/TweenLite': 'packages/greensock/TweenLite.min',
      // TweenLite: 'packages/greensock/TweenLite.min'
    }
  },
  shim: {
    'components/VideoPlayerYT': ['jquery', 'lib/Sequencer']
  },
  packages: [
    {
      name: 'moment',
      location: 'packages',
      main: 'moment'
    }
  ]
});

require([
  'jQuery',
  'lib/Modernizr',
  'underscore',
  // 'lib/Sequencer',
  'utils/resizer',
  'utils/Preloader',
  'lib/Tracking',
  'packages/slick.min',
  'packages/wow.min',
  'utils/Sniffer',
  'packages/Categorizr',
  'components/VideoPlayerYT',
  'moment',
  'packages/moment-timezone-with-data',
  'utils/resizer',
  'utils/Sniffer',
  'components/VideoDiv',
  'src/site'
], function($) {
  'use strict';

  SITE = SITE || {};
  SITE.app = SITE.app || {};

  function Main() {
    if ((sniffer.isIE && sniffer.v < 9) || (sniffer.isiOS && sniffer.v <= 6)) {
      addUpgradeNotice();
      return;
    }
    if (sniffer.isIE && sniffer.v == 10) {
      $('body').addClass('ie10');
    }
    // SITE.utils.history.init();

    if (sniffer.device == 'mobile') {
      SITE.settings.backgroundVideosEnabled = false;
    }
    // desktop, tablet, mobile - which are we on?
    SITE.settings.browser = categorizr();

    // our initial list of items that
    resizer.init({
      targets: [{ label: 'window', target: $(window) }, { label: 'shell', target: $('#shell') }]
    });

    resizer.addListener({ func: onResize, label: 'main' });

    document.addEventListener(visibilityEvent, function(event) {
      if (!document[hidden]) {
        handleWindowFocus();
      } else {
        onExitLink();
      }
    });
    $(window)
      .focus(function() {
        handleWindowFocus();
      })
      .blur(function() {
        onExitLink();
      });
    var list = [
      // { func: SITE.app.VideoPlayerYT.downloadYoutube },
      // { func: initSounds },
      // { func: loadSite },
      { func: initSite },
      { func: enterSite },
      { func: onSiteEntered }
    ];
    SITE.utils.sequencer.add(list);
  }

  /**
   * Find the appropriate release date & inject it over the default date
   * parse the list until we find the closest date to present
   */
  function startCountdown() {
    var now = new Date();
    function getTimeRemaining(endtime) {
      var t = Date.parse(endtime) - Date.parse(new Date());
      var seconds = Math.floor((t / 1000) % 60);
      var minutes = Math.floor((t / 1000 / 60) % 60);
      var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
      var days = Math.floor(t / (1000 * 60 * 60 * 24));
      return {
        total: t,
        days: days,
        hours: hours,
        minutes: minutes,
        seconds: seconds
      };
    }

    function initializeClock(id, endtime) {
      var clock = document.getElementById(id);
      var daysSpan = clock.querySelector('.days');
      var hoursSpan = clock.querySelector('.hours');
      var minutesSpan = clock.querySelector('.minutes');
      var secondsSpan = clock.querySelector('.seconds');

      function updateClock() {
        var t = getTimeRemaining(endtime);

        daysSpan.innerHTML = t.days;
        hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
        minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
        secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

        if (t.total <= 0) {
          clearInterval(timeinterval);
        }
      }

      updateClock();
      var timeinterval = setInterval(updateClock, 1000);
    }
    // define(['packages/moment-timezone'], function(moment) {
    var moment = require('moment');
    var timeAdj = moment(SITE.settings.launchDate)
      .tz('Europe/Helsinki')
      .format();
    var deadline = new Date(Date.parse(timeAdj));
    // var deadline = new Date(Date.parse(new Date(SITE.settings.launchDate)));
    initializeClock('countdown', deadline);
    initializeClock('countdown2', deadline);
    // });
  }

  /**
   * Okay we have our JSON, now we need to load the site assets
   * @param {Type}
   */
  function loadSite() {
    // initialize our actual preloader
    // SITE.utils.preloader.init({
    //   onComplete: handleLoadComplete,
    //   onProgress: handleLoadProgress
    // });
    // add the content that our preloader should actually load to cache the content
    // Each section can have its own set of files. Just add them to the list.
    // var preloadFiles = [];
    //
    //
    // _.each(SITE.settings.sections, function(o, i) {
    // 	if (o.enabled && o.preloadFiles) {
    // 		if (i === SITE.settings.curSection && o.postLoadFiles) {
    // 			if (o.postLoadFilesMobile && sniffer.device === 'mobile') {
    // 				o.postLoadFiles = o.postLoadFilesMobile;
    // 			}
    // 			preloadFiles.push(o.postLoadFiles);
    // 		}
    // 		preloadFiles.push(o.preloadFiles);
    // 	}
    // });
    // SITE.utils.preloader.add.apply(null, preloadFiles);
    //       SITE.app.preloadDisplay.init({
    //           onEnter: onPreloadEntered,
    //           onExit: onPreloadExited
    //       });
    //
    //       SITE.app.preloadDisplay.enter();
  }

  function onPreloadEntered() {
    SITE.utils.preloader.start();
  }
  /**
   * this is progress for the actual preloader itself. It just tells the display to fill up completely
   */
  function onPreloadExited() {
    SITE.utils.sequencer.next();
  }

  /**
   * this is a progress event for the preloader itself. It tells the preload display to update to the new %
   */
  function handleLoadComplete(e) {
    SITE.app.preloadDisplay.update(1);
  }

  function handleLoadProgress(e) {
    SITE.app.preloadDisplay.update(e.completedCount / e.totalCount);
  }

  /**
   * Actual Site initialization ---- this occurs AFTER preloading and BEFORE site entry
   * @param {Type}
   */
  function initSite() {
    SITE.app.VideoPlayerYT.downloadYoutube();
    SITE.sections.home.init();
    enterSite();
  }

  /**
   * We call for the site entry here.
   * @param {Type}
   */
  function enterSite() {
    // startCountdown();
    $('body').css('display', 'block');
    SITE.sections.home.enter();

    // onSiteEntered();
  }

  function onSiteEntered() {
    //
  }

  function addUpgradeNotice() {
    $('#preloader').append(
      '<div class="C" id="upgrade"><img id="logoU" src="images/cma_logo.svg">' + SITE.settings.upgrade + '</div>'
    );
  }

    function onResize(sizes) {
      if (sniffer.device !== 'desktop') {
        var shellH, footerT;
        if (sniffer.isAndroid) {
          shellH = window.screen.height - 100;
          footerT = window.screen.height;
          $('#sectionWrapper').css('height', shellH);
        } else {
          shellH = sizes.window.height - 125;
          footerT = sizes.window.height;
        }
        $('#sectionWrapper').css('min-height', shellH);
      }
    }
  //   function slideToSection() {
  //     new TimelineMax({
  //       immediateRender: true,
  //       onComplete: function() {
  //         SITE.utils.sequencer.next();
  //       }
  //     })
  //       .fromTo('#' + SITE.settings.lastSection, 0.9, { x: '0%' }, { x: '-100%', ease: Expo.easeInOut })
  //       .fromTo('#' + SITE.settings.curSection, 0.9, { x: '100%' }, { x: '0%', ease: Expo.easeInOut }, 0);
  //   }

  // site focus --------------------------------------------------
  var exitWasMuted = false;
  function onExitLink() {
    if (SITE.settings.curSection == 'videos') {
      SITE.sections.videos.pause();
      exitWasMuted = true;
    } else {
      exitWasMuted = $('body').hasClass('muted');
    }
    // autoMute();
  }

  function handleWindowFocus() {
    if (!exitWasMuted) {
      // autoUnmute();
    }
  }

  /* visibility api ---------------------------------- */
  // Get Browser-Specifc Prefix
  function getBrowserPrefix() {
    // Check for the unprefixed property.
    if ('hidden' in document) {
      return null;
    }

    // All the possible prefixes.
    var browserPrefixes = ['moz', 'ms', 'o', 'webkit'];

    for (var i = 0; i < browserPrefixes.length; i++) {
      var prefix = browserPrefixes[i] + 'Hidden';
      if (prefix in document) {
        return browserPrefixes[i];
      }
    }
    // The API is not supported in browser.
    return null;
  }

  // Get Browser Specific Hidden Property
  function hiddenProperty(prefix) {
    if (prefix) {
      return prefix + 'Hidden';
    } else {
      return 'hidden';
    }
  }

  // Get Browser Specific Visibility State
  function visibilityState(prefix) {
    if (prefix) {
      return prefix + 'VisibilityState';
    } else {
      return 'visibilityState';
    }
  }

  // Get Browser Specific Event
  function visibilityEvent(prefix) {
    if (prefix) {
      return prefix + 'visibilitychange';
    } else {
      return 'visibilitychange';
    }
  }

  var prefix = getBrowserPrefix(),
    hidden = hiddenProperty(prefix),
    visibilityState = visibilityState(prefix),
    visibilityEvent = visibilityEvent(prefix);
  $(function() {
    // Main.prototype.slideToSection = slideToSection;
    SITE.main = new Main();
  });
  // $(document).ready(init);
});
